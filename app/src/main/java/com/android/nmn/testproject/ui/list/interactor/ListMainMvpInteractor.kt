package com.android.nmn.testproject.ui.list.interactor

import com.android.nmn.testproject.ui.base.interactor.MVPInteractor
import com.google.gson.JsonObject
import io.reactivex.Observable

interface ListMainMvpInteractor : MVPInteractor {

    fun getAdvertList(page: Int): Observable<JsonObject>

}