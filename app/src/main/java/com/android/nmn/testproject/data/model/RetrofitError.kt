package com.android.nmn.testproject.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class RetrofitError(
        @Expose
        @SerializedName("statusCode")
        val statusCode: Int = 0)