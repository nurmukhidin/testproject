package com.android.nmn.testproject.data.prefs


interface PreferenceHelper {

    fun getCurrentUserName(): String

    fun setCurrentUserName(userName: String)

    fun getAccessToken(): String

    fun setAccessToken(token: String)
}