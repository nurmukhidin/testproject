package com.android.nmn.testproject.data.model

import android.support.v7.util.DiffUtil
import com.google.gson.annotations.SerializedName

data class Advert(@SerializedName("price_from")
                  val priceFrom: Int = 0,
                  @SerializedName("address")
                  val address: String = "",
                  @SerializedName("city")
                  val city: String = "",
                  @SerializedName("price_to")
                  val priceTo: Int = 0,
                  @SerializedName("name")
                  val name: String = "",
                  @SerializedName("description")
                  val description: String = "",
                  @SerializedName("id")
                  val id: Int = 0,
                  @SerializedName("url")
                  val url: String = ""){
    object DIFF_CALLBACK: DiffUtil.ItemCallback<Advert>(){
        override fun areItemsTheSame(p0: Advert, p1: Advert): Boolean {
            return p0.id == p1.id
        }

        override fun areContentsTheSame(p0: Advert, p1: Advert): Boolean {
            return p0.id == p1.id
        }
    }
}