package com.android.nmn.testproject.utils


object AppConstants {

    internal const val BASE_URL = "http://vup.kz/"
    internal const val RESULT = "result"

    internal const val DATA = "data"
    internal val APP_DB_NAME = "testproject"
    internal val PREF_NAME = "testproject"


}