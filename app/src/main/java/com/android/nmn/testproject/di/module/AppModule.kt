package com.android.nmn.testproject.di.module

import android.app.Application
import android.content.Context
import com.android.nmn.testproject.data.network.ApiHeader
import com.android.nmn.testproject.data.network.ApiHelper
import com.android.nmn.testproject.data.network.AppApiHelper
import com.android.nmn.testproject.data.prefs.AppPreferenceHelper
import com.android.nmn.testproject.data.prefs.PreferenceHelper
import com.android.nmn.testproject.di.ApiKeyInfo
import com.android.nmn.testproject.di.PreferenceInfo
import com.android.nmn.testproject.utils.AppConstants
import com.android.nmn.testproject.utils.SchedulerProvider
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Singleton


@Module
class AppModule {
    @Provides
    @Singleton
    internal fun provideContext(application: Application): Context = application

    @Provides
    @ApiKeyInfo
    internal fun provideApiKey(): String = "VKkzApiKey"

    @Provides
    @PreferenceInfo
    internal fun provideprefFileName(): String = AppConstants.PREF_NAME

    @Provides
    @Singleton
    internal fun providePrefHelper(appPreferenceHelper: AppPreferenceHelper): PreferenceHelper = appPreferenceHelper

    @Provides
    @Singleton
    internal fun provideProtectedApiHeader(@ApiKeyInfo apiKey: String, preferenceHelper: PreferenceHelper)
            : ApiHeader.ProtectedApiHeader = ApiHeader.ProtectedApiHeader(apiKey = apiKey,
            userId = preferenceHelper.getCurrentUserName(),
            accessToken = preferenceHelper.getCurrentUserName())

    @Provides
    @Singleton
    internal fun provideApiHelper(appApiHelper: AppApiHelper): ApiHelper = appApiHelper


    @Provides
    internal fun provideCompositeDisposable(): CompositeDisposable = CompositeDisposable()

    @Provides
    internal fun provideSchedulerProvider(): SchedulerProvider = SchedulerProvider()


}