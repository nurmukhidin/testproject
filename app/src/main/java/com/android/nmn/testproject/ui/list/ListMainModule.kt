package com.android.nmn.testproject.ui.list

import com.android.nmn.testproject.ui.list.interactor.ListMainInteractor
import com.android.nmn.testproject.ui.list.interactor.ListMainMvpInteractor
import com.android.nmn.testproject.ui.list.presenter.ListMainMvpPresenter
import com.android.nmn.testproject.ui.list.presenter.ListMainPresenter
import com.android.nmn.testproject.ui.list.view.AdvertAdapter
import com.android.nmn.testproject.ui.list.view.ListMainMvpView
import dagger.Module
import dagger.Provides

@Module
class ListMainModule {

    @Provides
    internal fun provideListMainInteractor(interactor: ListMainInteractor): ListMainMvpInteractor = interactor

    @Provides
    internal fun providePasswordPresenter(presenter: ListMainPresenter<ListMainMvpView, ListMainMvpInteractor>)
            : ListMainMvpPresenter<ListMainMvpView, ListMainMvpInteractor> = presenter

    @Provides
    internal fun provideAdvertAdapter(): AdvertAdapter = AdvertAdapter()
}