package com.android.nmn.testproject.ui.base.view


interface MVPView {

    fun showProgress()

    fun hideProgress()

    fun showMessage(message: String)

}