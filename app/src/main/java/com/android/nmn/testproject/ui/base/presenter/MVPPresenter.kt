package com.android.nmn.testproject.ui.base.presenter

import com.android.nmn.testproject.ui.base.interactor.MVPInteractor
import com.android.nmn.testproject.ui.base.view.MVPView

interface MVPPresenter<V : MVPView, I : MVPInteractor> {

    fun onAttach(view: V?)

    fun onDetach()

    fun getView(): V?

}