package com.android.nmn.testproject.ui.list.utils

import android.arch.paging.PageKeyedDataSource
import android.util.Log
import com.android.nmn.testproject.data.model.Advert
import com.android.nmn.testproject.data.network.ApiModelHelper
import com.android.nmn.testproject.ui.list.interactor.ListMainMvpInteractor
import com.android.nmn.testproject.utils.SchedulerProvider

class AdvertPageKeyedDataSource(val interactor: ListMainMvpInteractor,
                                val schedulerProvider: SchedulerProvider): PageKeyedDataSource<Int, Advert>() {

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, Advert>
    ) {
        interactor.let {
            it.getAdvertList(0)
                .compose(schedulerProvider.ioToMainObservableScheduler())
                .subscribe({
                    Log.i("ADDD", "$it")
                    val advertList = ApiModelHelper.getPageArray(it, Advert::class.java)
                    callback.onResult(advertList, null, 1)
                }, {
                    it.printStackTrace()
                })
        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Advert>) {
        Log.i("PAGGGG", params.key.toString())
        interactor.let {
            it.getAdvertList(params.key)
                .compose(schedulerProvider.ioToMainObservableScheduler())
                .subscribe({
                    Log.i("ADDDss", "$it")
                    val advertList = ApiModelHelper.getPageArray(it, Advert::class.java)
                    callback.onResult(advertList, params.key + 1)
                }, {
                    it.printStackTrace()
                })
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Advert>) {

    }
}