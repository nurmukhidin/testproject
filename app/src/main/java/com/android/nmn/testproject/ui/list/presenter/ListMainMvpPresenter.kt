package com.android.nmn.testproject.ui.list.presenter

import com.android.nmn.testproject.ui.base.presenter.MVPPresenter
import com.android.nmn.testproject.ui.list.interactor.ListMainMvpInteractor
import com.android.nmn.testproject.ui.list.utils.AdvertPageKeyedDataSource
import com.android.nmn.testproject.ui.list.view.ListMainMvpView


interface ListMainMvpPresenter<V : ListMainMvpView, I : ListMainMvpInteractor> : MVPPresenter<V, I> {

    fun getAdvertList(): AdvertPageKeyedDataSource

}