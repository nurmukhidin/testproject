package com.android.nmn.testproject.data.network

import com.android.nmn.testproject.utils.AppConstants


object RestApiRoute {

    const val ADVERT_LIST: String = "${AppConstants.BASE_URL}api/service/all"

}