package com.android.nmn.testproject.di.component

import android.app.Application
import com.android.nmn.testproject.app.TestApp
import com.android.nmn.testproject.di.builder.ActivityBuilder
import com.android.nmn.testproject.di.module.AppModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [(AndroidSupportInjectionModule::class), (AppModule::class), (ActivityBuilder::class)])
interface AppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(app: TestApp)

}