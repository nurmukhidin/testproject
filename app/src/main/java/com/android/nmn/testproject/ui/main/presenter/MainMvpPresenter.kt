package com.android.nmn.testproject.ui.main.presenter

import com.android.nmn.testproject.ui.base.presenter.MVPPresenter
import com.android.nmn.testproject.ui.main.interactor.MainMvpInteractor
import com.android.nmn.testproject.ui.main.view.MainMvpView

interface MainMvpPresenter<V : MainMvpView, I : MainMvpInteractor> : MVPPresenter<V, I> {
}