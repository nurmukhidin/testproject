package com.android.nmn.testproject.data.network

import com.google.gson.JsonObject
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AppApiHelper
@Inject constructor(private val apiHeader: ApiHeader): ApiHelper {

    override fun getAdvertList(page: Int): Observable<JsonObject> = RestApi.getApi().getAdvertList(page)

}