package com.android.nmn.testproject.di.builder

import com.android.nmn.testproject.ui.list.ListMainProvider
import com.android.nmn.testproject.ui.main.MainModule
import com.android.nmn.testproject.ui.main.view.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [
        (MainModule::class),
        (ListMainProvider::class)
    ])
    abstract fun bindMainActivity(): MainActivity

}