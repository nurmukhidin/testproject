package com.android.nmn.testproject.data.network

import com.google.gson.JsonObject
import io.reactivex.Observable
import retrofit2.http.*

interface ApiHelper {

    @GET(RestApiRoute.ADVERT_LIST)
    fun getAdvertList(@Query("page") page: Int): Observable<JsonObject>

}