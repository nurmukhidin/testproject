package com.android.nmn.testproject.ui.list

import com.android.nmn.testproject.ui.list.view.ListMainFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
internal abstract class ListMainProvider {

    @ContributesAndroidInjector(modules = [ListMainModule::class])
    internal abstract fun provideListMainFragmentFactory(): ListMainFragment
}