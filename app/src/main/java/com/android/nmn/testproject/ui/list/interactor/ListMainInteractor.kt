package com.android.nmn.testproject.ui.list.interactor

import com.android.nmn.testproject.data.network.ApiHelper
import com.android.nmn.testproject.data.prefs.PreferenceHelper
import com.android.nmn.testproject.ui.base.interactor.BaseInteractor
import com.google.gson.JsonObject
import io.reactivex.Observable
import javax.inject.Inject

class ListMainInteractor
@Inject internal constructor(preferenceHelper: PreferenceHelper, apiHelper: ApiHelper) :
    BaseInteractor(preferenceHelper = preferenceHelper, apiHelper = apiHelper), ListMainMvpInteractor {

    override fun getAdvertList(page: Int): Observable<JsonObject> = apiHelper.getAdvertList(page)

}