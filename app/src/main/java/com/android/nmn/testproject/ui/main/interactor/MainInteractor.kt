package com.android.nmn.testproject.ui.main.interactor

import com.android.nmn.testproject.data.network.ApiHelper
import com.android.nmn.testproject.data.prefs.PreferenceHelper
import com.android.nmn.testproject.ui.base.interactor.BaseInteractor
import javax.inject.Inject

class MainInteractor
@Inject internal constructor(preferenceHelper: PreferenceHelper, apiHelper: ApiHelper) :
    BaseInteractor(preferenceHelper = preferenceHelper, apiHelper = apiHelper), MainMvpInteractor {

}