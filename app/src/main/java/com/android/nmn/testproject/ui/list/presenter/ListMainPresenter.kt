package com.android.nmn.testproject.ui.list.presenter

import com.android.nmn.testproject.ui.base.presenter.BasePresenter
import com.android.nmn.testproject.ui.list.interactor.ListMainMvpInteractor
import com.android.nmn.testproject.ui.list.utils.AdvertPageKeyedDataSource
import com.android.nmn.testproject.ui.list.view.ListMainMvpView
import com.android.nmn.testproject.utils.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class ListMainPresenter<V : ListMainMvpView, I : ListMainMvpInteractor>
@Inject internal constructor(interactor: I, schedulerProvider: SchedulerProvider, disposable: CompositeDisposable) :
    BasePresenter<V, I>(interactor = interactor, schedulerProvider = schedulerProvider, compositeDisposable = disposable),
        ListMainMvpPresenter<V, I>  {

    override fun getAdvertList(): AdvertPageKeyedDataSource {
        return AdvertPageKeyedDataSource(interactor as ListMainMvpInteractor, schedulerProvider)
    }

}