package com.android.nmn.testproject.ui.main.presenter

import com.android.nmn.testproject.ui.base.presenter.BasePresenter
import com.android.nmn.testproject.ui.main.interactor.MainMvpInteractor
import com.android.nmn.testproject.ui.main.view.MainMvpView
import com.android.nmn.testproject.utils.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class MainPresenter <V : MainMvpView, I : MainMvpInteractor>
@Inject internal constructor(interactor: I, schedulerProvider: SchedulerProvider, disposable: CompositeDisposable) :
    BasePresenter<V, I>(interactor = interactor, schedulerProvider = schedulerProvider, compositeDisposable = disposable),
    MainMvpPresenter<V, I> {
}