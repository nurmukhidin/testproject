package com.android.nmn.testproject.ui.base.interactor

import com.android.nmn.testproject.data.network.ApiHelper
import com.android.nmn.testproject.data.prefs.PreferenceHelper

open class BaseInteractor() : MVPInteractor {

    protected lateinit var preferenceHelper: PreferenceHelper
    protected lateinit var apiHelper: ApiHelper

    constructor(preferenceHelper: PreferenceHelper, apiHelper: ApiHelper) : this() {
        this.preferenceHelper = preferenceHelper
        this.apiHelper = apiHelper
    }



}