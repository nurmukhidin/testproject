package com.android.nmn.testproject.data.network

import com.android.nmn.testproject.data.model.RetrofitError
import com.android.nmn.testproject.utils.AppConstants
import com.google.gson.Gson
import com.google.gson.JsonObject
import retrofit2.HttpException
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

object ApiModelHelper{

    internal fun <T> getObject(jsonObject: JsonObject, clazz: Class<T>): T{
        return Gson().fromJson(jsonObject.getAsJsonObject(AppConstants.RESULT).toString(), clazz)
    }

    internal fun <T> getArray(jsonObject: JsonObject, dataClass: Class<T>): MutableList<T> {
        return Gson().fromJson(jsonObject.getAsJsonArray(AppConstants.RESULT).toString(), getType(MutableList::class.java, dataClass))
    }

    internal fun <T> getPageArray(jsonObject: JsonObject, dataClass: Class<T>): MutableList<T> {
        return Gson().fromJson(jsonObject.getAsJsonObject(AppConstants.RESULT).getAsJsonArray("vacancy").toString(), getType(MutableList::class.java, dataClass))
    }

    internal fun getErrorStatusCode(throwable: Throwable): Int{
        var statusCode = 0

        if (throwable is HttpException){
            val body = throwable.response().errorBody()
            val str: String = body?.string().toString()
            val retrofitError = Gson().fromJson(str, RetrofitError::class.java)

            if (retrofitError != null) {
                statusCode = retrofitError.statusCode
            }
        }

        return statusCode
    }

    fun getType(cll: Class<*>, param: Class<*>): Type{
        return object : ParameterizedType {
            override fun getRawType(): Type {
                return cll
            }

            override fun getOwnerType(): Type? {
                return null
            }

            override fun getActualTypeArguments(): Array<Type> {
                return Array(1){param}
            }

        }
    }
}