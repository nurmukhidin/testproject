package com.android.nmn.testproject.data.prefs

import android.content.Context
import android.content.SharedPreferences
import com.android.nmn.testproject.di.PreferenceInfo
import javax.inject.Inject

class AppPreferenceHelper
@Inject constructor(context: Context,
                    @PreferenceInfo private val prefFileName: String) : PreferenceHelper {

    companion object {
        private val PREF_KEY_CURRENT_USER_NAME = "PREF_KEY_CURRENT_USER_NAME"
        private val PREF_TOKEN = "PREF_TOKEN"
        val NO_VAL = "PREF_NO_VAL"
    }

    private val mPrefs: SharedPreferences = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE)


    override fun getCurrentUserName(): String = mPrefs.getString(PREF_KEY_CURRENT_USER_NAME, NO_VAL)!!

    override fun setCurrentUserName(userName: String) { mPrefs.edit().putString(PREF_KEY_CURRENT_USER_NAME, userName).apply() }

    override fun getAccessToken(): String = mPrefs.getString(PREF_TOKEN, NO_VAL)!!

    override fun setAccessToken(token: String) { mPrefs.edit().putString(PREF_TOKEN, token).apply() }
}