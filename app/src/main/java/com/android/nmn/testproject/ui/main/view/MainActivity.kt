package com.android.nmn.testproject.ui.main.view

import android.os.Bundle
import android.support.v4.app.Fragment
import com.android.nmn.testproject.R
import com.android.nmn.testproject.ui.base.view.BaseActivity
import com.android.nmn.testproject.ui.list.view.ListMainFragment
import com.android.nmn.testproject.ui.main.interactor.MainMvpInteractor
import com.android.nmn.testproject.ui.main.presenter.MainMvpPresenter
import com.android.nmn.testproject.utils.extension.removeFragment
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class MainActivity: BaseActivity(), MainMvpView, HasSupportFragmentInjector{

    @Inject
    internal lateinit var presenter: MainMvpPresenter<MainMvpView, MainMvpInteractor>

    @Inject
    internal lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        presenter.onAttach(this)

        supportFragmentManager.beginTransaction()
            .replace(R.id.frameMain, ListMainFragment.newInstance())
            .commitAllowingStateLoss()
    }

    override fun supportFragmentInjector() = fragmentDispatchingAndroidInjector

    override fun onFragmentDetached(tag: String) {
        supportFragmentManager.removeFragment(tag  = tag)
    }

    override fun onFragmentAttached() {

    }

    override fun onDestroy() {
        presenter.onDetach()
        super.onDestroy()
    }
}
