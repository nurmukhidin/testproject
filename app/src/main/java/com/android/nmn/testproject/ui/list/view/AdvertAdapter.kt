package com.android.nmn.testproject.ui.list.view

import android.arch.paging.PagedListAdapter
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.android.nmn.testproject.R
import com.android.nmn.testproject.data.model.Advert
import com.android.nmn.testproject.utils.AppConstants
import com.bumptech.glide.Glide

class AdvertAdapter : PagedListAdapter<Advert, AdvertAdapter.MyViewHolder>(Advert.DIFF_CALLBACK) {

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): MyViewHolder {
        context = parent.context
        val view =  LayoutInflater.from(context).inflate(R.layout.adapter_advert, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: MyViewHolder, position: Int) {
        getItem(position).let {advert->
            viewHolder.apply {
                Glide.with(context)
                    .load("${AppConstants.BASE_URL}${advert!!.url}")
                    .error(R.drawable.no_image)
                    .into(imageView)

                titleView.text = advert.name
                addressView.text = advert.address

                val price = "${context.getString(R.string.from)} " +
                        "${advert.priceFrom} ${context.getString(R.string.tg)} " +
                        "${context.getString(R.string.until).toLowerCase()} " +
                        "${advert.priceTo} ${context.getString(R.string.tg)}"

                priceView.text = price
            }
        }

    }


    inner class MyViewHolder (view: View) : RecyclerView.ViewHolder(view) {

        val imageView: ImageView = view.findViewById(R.id.imgAdvertAdapter)
        val titleView: TextView = view.findViewById(R.id.txtTitleAdvertAdapter)
        val addressView: TextView = view.findViewById(R.id.txtAddressAdvertAdapter)
        val priceView: TextView = view.findViewById(R.id.txtPriceAdvertAdapter)
    }




}