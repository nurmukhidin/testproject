package com.android.nmn.testproject.di

import javax.inject.Qualifier


@Qualifier
@Retention annotation class PreferenceInfo