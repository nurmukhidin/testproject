package com.android.nmn.testproject.ui.main

import com.android.nmn.testproject.ui.main.interactor.MainInteractor
import com.android.nmn.testproject.ui.main.interactor.MainMvpInteractor
import com.android.nmn.testproject.ui.main.presenter.MainMvpPresenter
import com.android.nmn.testproject.ui.main.presenter.MainPresenter
import com.android.nmn.testproject.ui.main.view.MainMvpView
import dagger.Module
import dagger.Provides

@Module
class MainModule {

    @Provides
    internal fun provideMainInteractor(mainInteractor: MainInteractor): MainMvpInteractor = mainInteractor

    @Provides
    internal fun provideMainPresenter(mainPresenter: MainPresenter<MainMvpView, MainMvpInteractor>)
            : MainMvpPresenter<MainMvpView, MainMvpInteractor> = mainPresenter

}