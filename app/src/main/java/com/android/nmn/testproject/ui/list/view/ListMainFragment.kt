package com.android.nmn.testproject.ui.list.view

import android.arch.paging.PagedList
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.nmn.testproject.R
import com.android.nmn.testproject.data.model.Advert
import com.android.nmn.testproject.ui.base.view.BaseFragment
import com.android.nmn.testproject.ui.list.interactor.ListMainMvpInteractor
import com.android.nmn.testproject.ui.list.presenter.ListMainMvpPresenter
import com.android.nmn.testproject.utils.MainThreadExecutor
import dagger.android.DispatchingAndroidInjector
import kotlinx.android.synthetic.main.fragment_list.*
import java.util.concurrent.Executors
import javax.inject.Inject


class ListMainFragment : BaseFragment(), ListMainMvpView {

    companion object {

        internal val TAG = "ListMainFragment"

        fun newInstance():  ListMainFragment {
            return  ListMainFragment()
        }

    }

    @Inject
    internal lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    @Inject
    internal lateinit var presenter: ListMainMvpPresenter<ListMainMvpView, ListMainMvpInteractor>

    @Inject
    internal lateinit var adapter: AdvertAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        presenter.onAttach(this)
        super.onViewCreated(view, savedInstanceState)
    }

    override fun setUp() {

        val conf = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setPageSize(20)
            .build()
        val pagedList: PagedList<Advert> = PagedList.Builder(presenter.getAdvertList(), conf)
            .setNotifyExecutor(MainThreadExecutor())
            .setFetchExecutor(Executors.newSingleThreadExecutor())
            .build()
        adapter.submitList(pagedList)

        recyclerList.apply {
            adapter = this@ListMainFragment.adapter
            layoutManager = LinearLayoutManager(context)
        }
    }

}
